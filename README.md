# POKEDEX

Challenge Frontend

## Starting 🚀


See Deployment to learn how to deploy the project.


### Pre requirements 📋

requirement to have docker installed <a>
    <img width="40"  src="https://linuxcenter.es/images/icagenda/docker.png">
  </a>

```
[install docker](https://docs.docker.com/install/)
```

### Installation 🔧

What you must execute to have a development environment running


```
docker-compose up
```


## Despliegue 📦

To run in the frontend in the port 8080

```
http://localhost:8080/