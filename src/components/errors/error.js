import React from "react";

export default function Error({ text }) {
  return (
    <div className="error">
      <h1>{text}</h1>
    </div>
  );
}
