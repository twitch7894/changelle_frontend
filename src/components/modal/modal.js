import React, { useContext, useEffect } from "react";
import ReactDOM from "react-dom";
import LayoutModal from "../layout/layoutModal";
import { Context } from "../../containers/App";
import { colors } from "../../utils";
import Error from "../errors/error";
import Loading from "../loading/loading";

export default function Modal() {
  const { modalState } = useContext(Context);
  const { modal, data, error, loading } = modalState;

  return modal
    ? ReactDOM.createPortal(
        <LayoutModal>
          {loading ? (
            <Loading />
          ) : (
            <div className="container">
              <div
                className="detail_pokemon"
                style={{
                  backgroundColor: `${colors[data.types[0].type.name]}`,
                }}
              >
                <h1>{data.name}</h1>
                <img src={data.sprites.front_default} />
              </div>
              <div className="details-text">
                <p
                  className="number_pokemon"
                  style={{
                    backgroundColor: `${colors[data.types[0].type.name]}`,
                  }}
                >
                  #{data.id.toString().padStart(3, "0")}
                </p>
                <h2>Abilities</h2>
                {data.abilities.map((e) => (
                  <p key={e.ability.name}>{e.ability.name}</p>
                ))}
                <h2>Stats</h2>
                {data.stats.map((e) => (
                  <p key={e.stat.name}>
                    {e.stat.name} - {e.base_stat}
                  </p>
                ))}
              </div>
            </div>
          )}
          {error ? <Error text={error} /> : null}
        </LayoutModal>,
        document.body
      )
    : null;
}
