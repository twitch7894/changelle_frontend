import React, { useContext } from "react";
import { Context } from "../../containers/App";
import { CLOSE_MODAL } from "../../constants";

export default function LayoutModal({ children }) {
  const { modalDispatch } = useContext(Context);

  return (
    <React.Fragment>
      <div className="modal-overlay" />
      <div
        className="modal-wrapper"
        aria-modal
        aria-hidden
        tabIndex={-1}
        role="dialog"
      >
        <div className="modal">
          <div className="modal-header">
            <button
              type="button"
              className="modal-close-button"
              data-dismiss="modal"
              aria-label="Close"
              onClick={() => modalDispatch({ type: CLOSE_MODAL })}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          {children}
        </div>
      </div>
    </React.Fragment>
  );
}
