import React from "react";
import Img from "../../images/title.png";

export default function Header() {
  return (
    <div className="title_pokemon">
      <img src={Img} width={450} alt="title" />
    </div>
  );
}
