import React, { useContext, useState, useEffect } from "react";
import Axios from "axios";
import { Context } from "../../containers/App";
import { ENDPOINTS } from "../../config/endpoints";
import { FETCH_SUCCESS, FETCH_ERROR, INITIAL_FETCH } from "../../constants";

export default function Search() {
  const [text, setText] = useState("");
  const { GetPokemonState ,GetPokemonDispatch } = useContext(Context);
  const { data } = GetPokemonState;

  useEffect(() => {
    GetData();
  }, [ENDPOINTS.SEARCH(text), text]);

  async function GetData() {
    GetPokemonDispatch({ type: INITIAL_FETCH })
    if (text === "") { setText("");}
    if (text !== "" && data !== []) {
      try {
        const { data } = await Axios.get(ENDPOINTS.SEARCH(text));
        GetPokemonDispatch({
          type: FETCH_SUCCESS,
          payload: {
            results: [{ name: data.name, url: ENDPOINTS.SEARCH(text) }],
          },
        });
      } catch (err) {
          GetPokemonDispatch({ type: FETCH_ERROR });
      }
    } else  {
      try {
        const { data } = await Axios.get(ENDPOINTS.POKEMON);
        GetPokemonDispatch({ type: FETCH_SUCCESS, payload: data });
      } catch (err) {
        GetPokemonDispatch({ type: FETCH_ERROR });
      }
    }
  }

  const changeText = (e) => {
    setText(e.target.value.toLowerCase());
  };

  return (
    <div className="container_search">
      <span className="searchicon"></span>
      <input
        className="search"
        type="text"
        value={text}
        placeholder="Search"
        onChange={(e) => changeText(e)}
      />
    </div>
  );
}
