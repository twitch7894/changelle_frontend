import React, { useContext } from "react";
import Axios from "axios";
import { Context } from "../../containers/App";
import { FETCH_SUCCESS, FETCH_ERROR, INITIAL_FETCH } from "../../constants";

export default function Pagination() {
  const { GetPokemonState, GetPokemonDispatch } = useContext(Context);
  const { data } = GetPokemonState;

  async function ChangePaginate(types) {
    if (data[types] !== null) {
      GetPokemonDispatch({ type: INITIAL_FETCH });
      try {
        const response = await Axios.get(data[types]);
        GetPokemonDispatch({ type: FETCH_SUCCESS, payload: response.data });
      } catch (err) {
        GetPokemonDispatch({ type: FETCH_ERROR });
      }
    }
  }

  function displayPaginate() {
    if (data.length === 0 || data['previous'] === undefined) {
      return "none";
    } else {
      return "flex";
    }
  }
  return (
    <div className="pagination" style={{ display: `${displayPaginate()}` }}>
      <button className="button" onClick={() => ChangePaginate("previous")}>
        Previous
      </button>
      <button className="button" onClick={() => ChangePaginate("next")}>
        Next
      </button>
    </div>
  );
}
