import React, { useContext } from "react";
import Axios from "axios";
import { OPEN_MODAL, ERROR_MODAL, INITIAL_MODAL } from "../../constants";
import Img from "../../images/pokeball.png";
import { Context } from "../../containers/App";

export default function Card({ name, url }) {
  const { modalDispatch } = useContext(Context);

  async function GetData(url) {
    modalDispatch({ type: INITIAL_MODAL });
    try {
      const { data } = await Axios.get(url);
      modalDispatch({ type: OPEN_MODAL, payload: data });
    } catch (err) {
      modalDispatch({ type: ERROR_MODAL });
    }
  }

  return (
    <div className="card" onClick={() => GetData(url)}>
      <div className="card_img">
        <img src={Img} alt="img" />
      </div>
      <div className="card_info">
        <h3 className="card_name">{name}</h3>
      </div>
    </div>
  );
}
