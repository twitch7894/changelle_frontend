import React, { useContext, useEffect } from "react";
import Axios from "axios";
import { Context } from "../../containers/App";
import { ENDPOINTS } from "../../config/endpoints";
import { FETCH_SUCCESS, FETCH_ERROR } from "../../constants";
import Loading from "../loading/loading";
import Card from "../card/card";
import Error from "../errors/error";

export default function Content() {
  const { GetPokemonState, GetPokemonDispatch } = useContext(Context);
  const { loading, error, data } = GetPokemonState;
  useEffect(() => {
    GetData();
  }, []);

  async function GetData() {
    try {
      const { data } = await Axios.get(ENDPOINTS.POKEMON);
      GetPokemonDispatch({ type: FETCH_SUCCESS, payload: data });
    } catch (err) {
      GetPokemonDispatch({ type: FETCH_ERROR });
    }
  }

  return (
    <div className="container">
      {loading ? (
        <Loading />
      ) : (
        <div className="item">
          {data.results.map((e) => (
            <Card key={e.name} name={e.name} url={e.url} />
          ))}
        </div>
      )}
      {error ? <Error text={error} /> : null}
    </div>
  );
}
