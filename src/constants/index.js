/**
 * VARIABLE TYPES
 */

/**
 * GET DATA
 */

export const FETCH_SUCCESS = "FETCH_SUCCESS";
export const FETCH_ERROR = "FETCH_ERROR";
export const INITIAL_FETCH = "INITIAL_FETCH";

/**
 *  MODAL
 */

export const OPEN_MODAL = "OPEN_MODAL";
export const CLOSE_MODAL = "CLOSE_MODAL";
export const ERROR_MODAL = "ERROR_MODAL";
export const INITIAL_MODAL = "INITIAL_MODAL";
