/**
 * ENDPOINTS POKEMON
 */

const HOST = "https://pokeapi.co/api/v2/pokemon";

export const ENDPOINTS = {
  POKEMON: `${HOST}/?offset=0&limit=5`,
  SEARCH: (id) => `${HOST}/${id}`,
};
