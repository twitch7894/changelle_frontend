import {
  OPEN_MODAL,
  CLOSE_MODAL,
  ERROR_MODAL,
  INITIAL_MODAL,
} from "../constants";

const initialState = {
  modal: false,
  loading: true,
  data: [],
  error: "",
};

const reducer = (state, actions) => {
  switch (actions.type) {
    case OPEN_MODAL:
      return {
        modal: true,
        loading: false,
        data: actions.payload,
      };
    case CLOSE_MODAL:
      return {
        modal: false,
        loading: false,
      };
    case INITIAL_MODAL:
      return initialState;
    case ERROR_MODAL:
      return {
        error: "Something went wrong!",
        loading: false,
      };
    default:
      return state;
  }
};

export const Modal = { initialState, reducer };
