import React, { useReducer } from "react";
import { GetPokemon } from "../reducers/getPokemon";
import { Modal } from "../reducers/modal";
import Pokedex from "./Pokedex";

export const Context = React.createContext();

function App() {
  const [GetPokemonState, GetPokemonDispatch] = useReducer(
    GetPokemon.reducer,
    GetPokemon.initialState
  );
  const [modalState, modalDispatch] = useReducer(
    Modal.reducer,
    Modal.initialState
  );

  // Reducers
  const reducers = {
    GetPokemonState,
    GetPokemonDispatch,
    modalState,
    modalDispatch,
  };
  return (
    <Context.Provider value={reducers}>
      <Pokedex />
    </Context.Provider>
  );
}

export default App;
