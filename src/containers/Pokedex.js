import React from "react";
import Header from "../components/header/header";
import Search from "../components/search/search";
import Content from "../components/content/content";
import Pagination from "../components/pagination/pagination";
import Modal from "../components/modal/modal";

export default function Pokedex() {
  return (
    <div className="container">
      <Header />
      <Search />
      <Content />
      <Pagination />
      <Modal />
    </div>
  );
}
