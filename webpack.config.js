const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpackMerge = require("webpack-merge");
const path = require('path');
const loadPresets = require("./build-utils/loadPresets");
const modeConfig = env => require(`./build-utils/webpack.${env.mode}.js`)(env);
const APP_DIR = path.resolve(__dirname, './src'); // <===== new stuff added here

module.exports = ({ mode, presets } = { mode: "production", presets: [] }) => {
  console.log(mode);
  return webpackMerge(
    {
      mode,
      entry: ['@babel/polyfill', APP_DIR], // <===== new stuff added here
      module: {
        rules: [
          {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {
              loader: "babel-loader",
              options: {
                presets: ['@babel/preset-env', '@babel/preset-react']
              }
            }
          },
          {
            test: /\.(jpg|png|gif|svg)$/,
            use: {
              loader: 'url-loader',
              options: {
                limit: 10000,
                fallback: 'file-loader',
                name: 'assets/images/[name].[hash].[ext]',
              }
            }
          },
          {
            test: /\.ts$/,
            use: "ts-loader"
          }
        ]
      },
      plugins: [new webpack.ProgressPlugin(), new HtmlWebpackPlugin({
        template: "./src/index.html",
        filename: "./index.html"
      })
      ]
    },
    modeConfig({ mode, presets }),
    loadPresets({ mode, presets })
  );
};